/*
* Copyright 2015-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
*
*     http://aws.amazon.com/apache2.0/
*
* or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/
package authorizer;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.okta.jwt.AccessTokenVerifier;
import com.okta.jwt.Jwt;
import com.okta.jwt.JwtVerificationException;
import com.okta.jwt.JwtVerifiers;
import io.AuthPolicy;
import io.TokenAuthorizerContext;


/**
 * Handles IO for a Java Lambda function as a custom authorizer for API Gateway
 * https://github.com/awslabs/aws-apigateway-lambda-authorizer-blueprints/tree/master/blueprints/java
 * @author Jack Kohn
 * Modified by David Mizrahi
 */
public class APIGatewayAuthorizerHandler implements RequestHandler<TokenAuthorizerContext, AuthPolicy> {

    private static final String issuer = System.getenv("issuer");
    private static final String audience = System.getenv("audience");

    private AccessTokenVerifier jwtVerifier = JwtVerifiers.accessTokenVerifierBuilder()
            .setIssuer(issuer)
            .setAudience(audience)
            .build();

    @Override
    public AuthPolicy handleRequest(TokenAuthorizerContext input, Context context) {

        LambdaLogger logger = context.getLogger();

        if (input.getAuthorizationToken() == null || input.getAuthorizationToken().equals("")) {
            logger.log("Authorization type not selected. Token wasn't presented.");
            throw new RuntimeException("Unauthorized. No token.");
        }

        String authHeader = input.getAuthorizationToken();
        String type = authHeader.substring(0, 7);

        if (!type.equals("Bearer ")) {
            logger.log("Authorization type 'Bearer'. Token wasn't presented.");
            throw new RuntimeException("Unauthorized. Token is empty");
        }
    	String token = authHeader.substring(7);

    	// validate the incoming token
        // and produce the principal user identifier associated with the token
        Jwt jwt;
        try {
            jwt = jwtVerifier.decode(token);
        } catch (JwtVerificationException e) {
            logger.log("Token: " + token);
            logger.log("Token parsing error: " + e.getMessage());
            throw new RuntimeException("Unauthorized");
        }

        String principalId = (String) jwt.getClaims().get("sub");
    	String methodArn = input.getMethodArn();
    	String[] arnPartials = methodArn.split(":");
    	String region = arnPartials[3];
    	String awsAccountId = arnPartials[4];
    	String[] apiGatewayArnPartials = arnPartials[5].split("/");
    	String restApiId = apiGatewayArnPartials[0];

    	String stage = "";
        if (apiGatewayArnPartials.length == 2) stage = apiGatewayArnPartials[1];

    	String httpMethod = "";
    	if (apiGatewayArnPartials.length == 3) httpMethod = apiGatewayArnPartials[2];

    	String resource = ""; // root resource
    	if (apiGatewayArnPartials.length == 4) resource = apiGatewayArnPartials[3];

    	return new AuthPolicy(principalId, AuthPolicy.PolicyDocument.getAllowAllPolicy(region, awsAccountId, restApiId, stage));
    }
}
